import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
    
import './App.css';
import "animate.css/animate.min.css";

import Main from './exec/Main.js';
import NavBar from './exec/NavBar.js';
import Blog from './exec/Blog.js';
import Login from './exec/Login.js';
import SignUp from './exec/SignUp.js';


class App extends React.Component{

    constructor(props){
      super(props);

      this.state={

      }
    }

    componentDidMount() {
      this.checkCurrentPage();  
    }

    checkCurrentPage(){

      let location = window.location.href;
        
      let mainLink = document.getElementById('mainLink');
      let orderLink = document.getElementById('orderLink');
      let blogLink = document.getElementById('blogLink');

      switch(location){

        // in case of the main page render
        case 'http://localhost:3000/main':{
          this.highlightAt(mainLink);
          this.highlightDisable(orderLink);
          this.highlightDisable(blogLink);
          break;
        }

        // in case of the order page render
        case 'http://localhost:3000/order':{
          this.highlightAt(orderLink);
          this.highlightDisable(mainLink);
          this.highlightDisable(blogLink);
          break;
        }

        case 'http://localhost:3000/blog':{
          this.highlightAt(blogLink);
          this.highlightDisable(mainLink);
          this.highlightDisable(orderLink);
          break;
        }

        default:{
          console.log('unexpected call');
        }
      }
    }

    highlightAt(linkToEnable){
        linkToEnable.setAttribute("style", "color:#b3a779");
    }

    highlightDisable(linkToDisable){
      linkToDisable.setAttribute("style", "color:none");
    }


    render(){
      return(
        <div>
          <Router>

            <Route exact path={'/main'} render={() => {return(<div><NavBar/><Main/></div>)}}/>
            <Route exact path={'/order'} render={() => {return(<div><NavBar/></div>)}}/>
            <Route exact path={'/blog'} render={() => {return(<div><NavBar/><Blog/></div>)}}/>
            <Route exact path={'/login'} render={() => {return(<div><Login/></div>)}}/>
            <Route exact path={'/signup'} render={() => {return(<div><SignUp/></div>)}}/>


          </Router>
        </div>
      )
    }

}

export default App;
