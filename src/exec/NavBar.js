import React from 'react';
import Icon from 'react-icons-kit';

import Headroom from 'react-headroom';
import {play} from 'react-icons-kit/entypo/play'
import '../css/NavBar.css'

class NavBar extends React.Component{

    constructor(props){
        super(props);

        this.state={}
    }

    render(){
        return(
            <Headroom style={{
                WebkitTransition: 'all .5s ease-in-out',
                MozTransition: 'all .5s ease-in-out',
                OTransition: 'all .5s ease-in-out',
                Transition: 'all .5s ease-in-out'
            }}>
                <section className = 'navbar'>
                    <a href='/main' id="mainLink">Главная</a>
                    <a href='/order' id="orderLink">Оформить заявку онлайн</a>
                    <a href='/blog' id="blogLink">Блог</a>

                    <Icon className='icon' size={32} icon={play}/>
                </section>
            </Headroom>
        )
    }

}

export default NavBar;
