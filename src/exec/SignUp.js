import React from 'react';
import '../css/SignUp.css';

import Icon from 'react-icons-kit';
import {arrows_remove} from 'react-icons-kit/linea/arrows_remove'

//TODO add check for second 

class SignUp extends React.Component{

    constructor(props){
        super(props);

        this.state={}
    }

    render(){
        return(
            <div>
                <section className='top'>
                    <Icon onClick={()=>{window.location.href='/main'}} size={70} icon={arrows_remove}/>
                </section>

                <section className='loginWrapper'>

                    <p id='enter'>Зарегистрироваться</p>
                    
                    <div className='txtWrapper'>
                        <p>Уже есть аккаунт?</p>
                        <p onClick={()=>{window.location.href='/login'}} id='register'>Войти</p>
                    </div>

                    <div className='inputs'>
                        <form class="go-bottom">
                                <div className='labelDiv'>
                                    <input id="name" type="text" required/>
                                    <label for="name">Эл.почта</label>
                                </div>

                                <div className='labelDiv'>
                                    <input id="phone" type="text" required/>
                                    <label for="phone">Имя пользователя</label>
                                </div>

                                <div className='labelDiv'>
                                    <input id="phone" type="password" required/>
                                    <label for="phone">Пароль</label>
                                </div>

                                <div className='labelDiv'>
                                    <input id="phone" type="password" required/>
                                    <label for="phone">Повторите пароль</label>
                                </div>
                        </form>
                    </div>

                    <button className='enter'>Зарегистрироваться</button>
                </section>
            </div>
        )
    }

}

export default SignUp;
