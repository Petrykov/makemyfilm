import React from 'react';
import Popup from "reactjs-popup";

import Icon from 'react-icons-kit';
import '../css/Blog.css';
import companyLogo from '../img/makeMyHor.jpg';
import Cookies from 'universal-cookie';
import LogIn from '../exec/Login.js';

import {search} from 'react-icons-kit/fa/search';
import {star} from 'react-icons-kit/iconic/star';
import {share} from 'react-icons-kit/icomoon/share';
import {calendar} from 'react-icons-kit/icomoon/calendar';
import {ic_watch_later} from 'react-icons-kit/md/ic_watch_later';
import {ic_mode_edit} from 'react-icons-kit/md/ic_mode_edit';
import {ic_delete} from 'react-icons-kit/md/ic_delete';

const cookies = new Cookies();
let xhr = new XMLHttpRequest();

let loggedInCookieValue;

let readTime, heading, data, alert, date, day, month, year;

class Blog extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            posts: [],
            adminAccount: {}
        }
    }

    init=()=>{
        readTime = document.getElementById('readingTime');
        heading = document.getElementById('heading');
        data = document.getElementById('data');
        alert = document.getElementById('alertAdd');
        date = new Date();
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();
    }

    componentDidMount() {
        loggedInCookieValue = cookies.get('isLoggedIn');
        if(loggedInCookieValue == 1){this.getAdmin()};
        this.getPosts();
    }

    getAdmin(){
        console.log("L: " + loggedInCookieValue);

            xhr.open('GET','/api/user/'+loggedInCookieValue, false);

            xhr.onreadystatechange = function(){
                if(xhr.readyState === 4){
                    if(xhr.status === 201){
                        console.log(JSON.parse(xhr.responseText));
                        this.setState({adminAccount:JSON.parse(xhr.responseText)});
                    }
                }
            }.bind(this);

            xhr.send();
    }

    getPosts() {

        xhr.open('GET', '/api/posts/', false);

        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 201) {
                    this.setState({posts: JSON.parse(xhr.responseText)});
                } 
            }
        }.bind(this);

        xhr.send();
    }

    logOut() {
        cookies.remove('isLoggedIn');
        window.location.href = '/blog';
    }

    deleteBlog() {

        let posts = document.getElementsByClassName('posts')[0].getElementsByClassName('singlePost');
        let xhr = new XMLHttpRequest();

            for(let i=0; i<posts.length; i++){
                posts[i].addEventListener('click',function(){
                
                    if(posts[i]!==undefined){

                        let id = posts[i].getAttribute('id').split('-');
                    
                        xhr.open('DELETE', '/api/posts/'+id[1], true);
                        xhr.onreadystatechange = () => {
                            if (xhr.readyState === 4) {
                                if (xhr.status === 201) {
                                // console.log(xhr.responseText);
                                  posts[i].remove();
                                }
                            }
                        }
                    xhr.send();
                    }
                })
            }  
    }

    insertBlog =()=>{

        this.init();
        
        if(!(this.checkInput(readTime) === false | this.checkInput(heading) === false | this.checkInput(data) === false)){            
            if(isNaN(readTime.value) === true){
                alert.setAttribute('style', 'border: 1px solid red;');
                alert.setAttribute('style', 'display:block;');
                alert.innerHTML = 'Read time field can not be a text';
                this.executeFor(5000,alert);
            }else{
                xhr.open("POST", '/api/posts', true);
                xhr.setRequestHeader("Content-type", "application/json");

                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 201) {
                            console.log(xhr.responseText);
                            window.location.href = '/blog';
                        } else {
                            this.setState({errorMessage: 'no user was found'});
                            alert.setAttribute('style', 'display:inherit;');
                            console.log("E: " + this.state.errorMessage);
                        }
                    }
                }.bind(this);

                const json = {
                    "userId": this.state.adminAccount.id,
                    "date": day+'.'+month+'.'+year,
                    "readTime": readTime.value,
                    "heading": heading.value,
                    "data": data.value,
                    "views": '0',
                };

                console.log(json);
    
                xhr.send(JSON.stringify(json));
            }
        }
    }

    checkInput(object){
        if(object.value.length === 0) {
            alert.setAttribute('style', 'display:block;');
            object.setAttribute('style', 'border: 0.5px solid red;');   
            this.executeFor(5000,alert);         
            return false;
        }else{
            object.setAttribute('style', 'border: 1px solid black');
            return true
        }
    }

    executeFor(time,object){
        setTimeout(function(){ 
            object.setAttribute('style', 'display:none;');
            object.innerHTML = 'Fields can not be empty';
        }, time);
    }

    render() {
        return (
            <div>
                
                <section className='header'>
                    <img alt='logo' src={companyLogo}/>
                </section>

                <section className='blogWrapper'>
                    <div className='topNav'>
                        <div className='text'>
                            <p>Все посты</p>
                        </div>

                        <div className='materials'>
                        
                            <Icon size={25} icon={search}/>
                            {(loggedInCookieValue == 1) ? <Icon onClick={this.deleteBlog} size={25} id='ic_delete' icon={ic_delete}/> : <div/>}
                            {(loggedInCookieValue == 1) ? <Icon size={25} id='ic_edit' icon={ic_mode_edit}/> : <div/>}
                            {(loggedInCookieValue == 1) ? 
                               
                               <Popup trigger={<button className='addBtn'>Add new</button>} position="bottom center">
                                    <div className='addInputs'>
                                        <input id='readingTime' type='text' placeholder='Reading time'/>
                                        <input id='heading' type='text' placeholder='Heading'/>
                                        <textarea id='data' type='text' placeholder='Data'/>
                                        <p id="alertAdd" >{'Fields can not be empty'}</p>
                                        <button onClick={this.insertBlog} id='confirm'>add</button>
                                    </div>
                                </Popup>:<div/>}    

                            {(loggedInCookieValue === undefined)? 
                            (<button id='login' onClick={() => {window.location.href = '/login'}}>Вход/Регистрация</button> )
                            :(<button id='logout' onClick={this.logOut}>Выйти</button>)}

                        </div>
                    </div>

                    <div className='posts'>

                        {this.state.posts.map((post, index) => {
                            index++;
                            let writter = post.writter;
                            return (
                                <div id={'id-' + post.id} key={'key-' + index} className='singlePost'>

                                    <div className='topSection'>

                                        <div className='inner_1'>
                                            {writter !== undefined && writter !== null ?
                                                <img alt='creator' src={writter.img}/> : 'loading...'}

                                        </div>

                                        <div className='inner_2'>
                                            <div className='wrapper_1'>
                                                {writter !== undefined && writter !== null ?
                                                    <p>{writter.username}</p> : 'loading...'}
                                                <Icon icon={star}/>
                                            </div>

                                            <div className='wrapper_2'>
                                                <Icon icon={calendar}/>
                                                <p>{post.date}</p>
                                                <Icon icon={ic_watch_later}/>
                                                <p>{post.readTime}</p>
                                            </div>
                                        </div>

                                        <div className='inner_3'>
                                            <Icon icon={share}/>
                                        </div>

                                    </div>

                                    <div className='mainSection'>
                                        <p id='headingTXT'>{post.heading}</p>
                                        <p id='mainTXT'>{post.data}</p>
                                    </div>

                                    <div className='footerSection'>
                                        <p>Просмотров: {post.views}</p>
                                        <p id='comment'>Комментировать</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </section>
            </div>
        )
    }

}

export default Blog;
