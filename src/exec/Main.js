import React from 'react';

import '../css/Main.css';

import companyLogo from '../img/makeMyHor.jpg';
import contactImg from '../img/contact.jpg';
import ScrollAnimation from 'react-animate-on-scroll';

let fName, address, email, phone, theme, addMessage, alertMessage;

class Main extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            paragraphFirst: 'Сейчас очень много снимают видео. Интересные события, путешествия, праздники... Сегодня даже обычная камера в вашем смартфоне дает такое качество картинки, о котором лет 5 назад можно было только мечтать. И, конечно же, хочется все это красиво оформить и сохранить.',
            paragraphSecond: 'Видеомонтаж не самый простой процесс,он занимает достаточно много времени, ведь весь материал нужно посмотреть, удалить неудачные моменты, последовательно выстроить кадры и связать их в одну, логическую историю. Мы занимаемся монтажом с 2014 года, за это время научились чувствовать потребность клиента и показывать хороший результат. Make my film-поможем оформить ваш фильм!',
            services: [
                {
                    video: 'https://www.youtube.com/embed/tgbNymZ7vqY',
                    s1: 'МОНТАЖ ВИДЕО С ВАШЕГО ПРАЗДНИКА',
                    s2: 'День рождение, семейный праздник, вечеринка, путешествие',
                    s3: 'Требуется Студия видеомонтажа с хорошей репутацией? Наши специалисты владеют всеми необходимыми навыками и средствами для решения любой задачи вне зависимости от объема.Мы гарантируем качество работы, пунктуальность и профессионализм.'
                },
                {
                    video: 'https://www.youtube.com/embed/tgbNymZ7vqY',
                    s1: 'ВИДЕОПОЗДРАВЛЕНИЕ',
                    s2: 'Оригинальное поздравление с праздником',
                    s3: 'У нас вы получите качественные услуги и достойный результат по доступной цене. Наши квалифицированные специалисты всегда используют в работе свои глубокие знания и богатый опыт. Наша компания выделяется среди конкурентов вниманием к деталям и потребностям каждого клиента.'
                },
                {
                    video: 'https://www.youtube.com/embed/tgbNymZ7vqY',
                    s1: 'ВИДЕО ДЛЯ СОЦСЕТЕЙ',
                    s2: 'Видео, как инструмент продвижения в соцсетях    ',
                    s3: 'Специалисты компании «make my film» оказывают услуги быстро и эффективно. Мы понимаем, насколько важно для клиента найти исполнителя, которому можно доверять. Наши сотрудники тщательно готовятся к работе, ведь каждый запрос уникален и требует особого подхода.'
                }
            ],
            alert: 'Заполните все поля прежде чем отправить'
        }
    }

    init() {
        fName = document.getElementById('fname');
        address = document.getElementById('address');
        email = document.getElementById('email');
        phone = document.getElementById('phone');
        theme = document.getElementById('theme');
        addMessage = document.getElementById('addMessage');
        alertMessage = document.getElementById('alert');
    }

    componentDidMount() {
        this.init();

        this.addEventListener(fName);
        this.addEventListener(address);
        this.addEventListener(email);
        this.addEventListener(phone);
        this.addEventListener(theme);
        this.addEventListener(addMessage);
    }

    addEventListener(value) {
        value.addEventListener('input', function () {
            (value.value.length === 0) ? value.setAttribute('style', 'border: 0.5px solid red;') : value.setAttribute('style', 'border: 0.5px solid black;');
        });
    }

    checkInputs = () => {
        if (this.singleInputCheck(fName) === true & this.singleInputCheck(address) === true & this.singleInputCheck(email) === true &
            this.singleInputCheck(phone) === true & this.singleInputCheck(theme) === true & this.singleInputCheck(addMessage) === true) {
            alertMessage.setAttribute('style', 'display:none;');
        } else {
            alertMessage.innerHTML = this.state.alert;
            alertMessage.setAttribute('style', 'display:flex;');
        }

        setTimeout(function(){ 
            alertMessage.innerHTML = '';
        }, 5000);
    }
    

    singleInputCheck = (value) => {
        let isEmpty;

        if (value.value.length === 0) {
            value.setAttribute('style', 'border: 0.5px solid red;');
            isEmpty = false;
        } else {
            value.setAttribute('style', 'border: 0.5px solid black;');
            isEmpty = true;
        }

        return isEmpty;
    }

    render() {
        return (
            <div>
                <section className='header'>
                    <div>
                        <img src={companyLogo} alt='myImg'/>
                    </div>

                </section>

                <section className="aboutUs">

                    <ScrollAnimation animateOnce={true} animateIn="fadeIn" duration={5}>
                        <p id='about'>ABOUT US</p>
                        <p id='trust'>Нам можно доверить ваши эмоции</p>
                        <p id='description'>{this.state.paragraphFirst}<br/><br/>{this.state.paragraphSecond}</p>
                    </ScrollAnimation>

                    <ScrollAnimation animateIn='flipInY' duration={0.5}>
                        <button>Связаться с нами</button>
                    </ScrollAnimation>

                </section>

                <section className="services">
                    <p id='services'>Наши услуги</p>

                    <div className="wrapper">

                        {this.state.services.map((obj, index) => {
                            return (
                                <div key={'key-' + index} className='singleService'>

                                    <ScrollAnimation animateIn='fadeIn' duration={1}>
                                        <iframe allow="fullscreen" title={'title-' + index} src={obj.video}/>
                                    </ScrollAnimation>

                                    <ScrollAnimation animateOnce={true} animateIn='fadeIn' duration={5}>
                                        <div className='textWrapper'>
                                            <p id='s1'>{obj.s1}</p>
                                            <p id='s2'>{obj.s2}</p>
                                            <p id='s3'>{obj.s3}</p>
                                        </div>
                                    </ScrollAnimation>
                                </div>
                            );
                        })}
                    </div>
                </section>

                <section className='contact'>

                    <div className='contactImg'>
                        <img alt='alt' src={contactImg}/>
                    </div>
                        
                    <div className='contactWrapper'>
                        <div className='contactForm'>
                            <p id='contactsTXT'>Контакты</p>
                            <p>Вы можете написать, позвонить нам, или оставить заявку<br/><br/>vlr.bereza@gmail.com</p>

                            <input type="text" id="fname" placeholder="Имя"/>
                            <input type="text" id="address" placeholder="Адрес"/>
                            <input type="text" id="email" placeholder="Эл.почта"/>
                            <input type="text" id="phone" placeholder="Телефон"/>
                            <input type="text" id="theme" placeholder="Тема"/>
                            <input type="text" id="addMessage" placeholder="Добавить сообщение"/>

                            <p id='alert'>{this.state.alert}</p>

                            <button id='send' onClick={this.checkInputs}>Отправить</button>
                        </div>
                    </div>

                </section>

            </div>
        )
    }

}

export default Main;
