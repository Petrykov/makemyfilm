import React from 'react';

import Cookies from 'universal-cookie';
import '../css/Login.css';
import '../css/Login.scss';
import Blog from '../exec/Blog.js';
import Icon from 'react-icons-kit';
import {arrows_remove} from 'react-icons-kit/linea/arrows_remove';

const cookies = new Cookies();

let email, password, alert;

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            errorMessage: '',
            userId: 1
        }
    }

    componentDidMount() {
        this.init();
    }

    init = () => {
        email = document.getElementById('email');
        password = document.getElementById('password');
        alert = document.getElementById('check');
    }

    logIn = () => {
        var xhr = new XMLHttpRequest();

        xhr.open("POST", '/api/log-in', true);
        xhr.setRequestHeader("Content-type", "application/json");

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 201) {
                    cookies.set('isLoggedIn', JSON.parse(xhr.responseText));
                    window.location.href = '/blog';
                } else {
                    this.setState({errorMessage: 'no user was found'});
                    alert.setAttribute('style', 'display:inherit;');
                    console.log("E: " + this.state.errorMessage);
                }
            }
        }.bind(this);

        if (this.checkInputs() === true) {
            const json = {
                "email": email.value,
                "password": password.value
            };

            console.log("J: " + json.email + ' | ' + json.password);

            xhr.send(JSON.stringify(json));
        }
    }

    checkInputs = () => {
        if (email.value === '') {
            this.setState({errorMessage: 'fill in the username firstly'});
            alert.setAttribute('style', 'display:inherit;');
            return false;
        } else if (password.value === '') {
            this.setState({errorMessage: 'fill in the password field'});
            alert.setAttribute('style', 'display:inherit;');
            return false;
        }

        return true;
    }

    render() {
        return (
            <div>
                <section className='top'>
                    <Icon onClick={() => {
                        window.location.href = '/main'
                    }} size={70} icon={arrows_remove}/>
                </section>

                <section className='loginWrapper'>
                    <p id='enter'>Войти</p>

                    <div className='txtWrapper'>
                        <p>Новичок на сайте?</p>
                        <p onClick={() => {
                            window.location.href = '/signup'
                        }} id='register'>Зарегистрироваться</p>
                    </div>

                    <div className='inputs'>
                        <form className="go-bottom">
                            <div className='labelDiv'>
                                <input id="email" type="text" required/>
                                <label>Эл.почта/Имя пользователя</label>
                            </div>

                            <div className='labelDiv'>
                                <input id="password" type="password" required/>
                                <label>Пароль</label>
                            </div>
                        </form>

                        <p id='check'>{this.state.errorMessage}</p>

                        <button onClick={this.logIn} className='enter'>Войти</button>                        
                    </div>
                </section>
            </div>
        )
    }

}

export default Login;
