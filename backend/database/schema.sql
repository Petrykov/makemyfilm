
-- TODO: implement array of comments

CREATE TABLE IF NOT EXISTS posts (
    id INTEGER PRIMARY KEY,
    userId INTEGER,
    writter TEXT,
    date TEXT,
    readTime DOUBLE,
    heading TEXT,
    data TEXT,
    views INTEGER,

    FOREIGN KEY (userId) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY,
    img TEXT,
    email TEXT, 
    username TEXT,
    password TEXT,
    type INT
);

INSERT OR REPLACE INTO posts(id,userId,date,readTime,heading,data,views)
VALUES(1,1,'28.02.2019',2.5,'Как сделать вирусное видео','Расскажите историю Убедитесь, что в вашем видео есть все основные составляющие повествования (завязка, персонажи, конфликт, кульминация и развязка) . Также в...',5);

INSERT OR REPLACE INTO posts(id,userId,date,readTime,heading,data,views)
VALUES(2,2,'25.05.2019',3,'Новая тема для блога','Текст samle text екст samle text text екст samle text text екст samle text',8);

INSERT OR REPLACE INTO users(id,img,email,username,password,type)
VALUES(1,'Valeria.png','valeriaBereza@gmail.com','vlbereza','pass',1);

INSERT OR REPLACE INTO users(id,img,email,username,password,type)
VALUES(2,'mercedesLogo.png','vladPetrykov@gmail.com','vpetrykov','passV',2);
