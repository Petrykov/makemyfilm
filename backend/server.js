var express = require("express");
const fs = require('fs');
const sqlite3 = require('sqlite3');
const bodyParser = require('body-parser');
const app = express();

var postsAPI = require('./routers/posts.js');
var clientAPI = require('./routers/client.js');

app.use(bodyParser.json());

// Server port
var HTTP_PORT = 5000;


function createDb(ready) {
    app.db = new sqlite3.Database('./database/sqlite3.db');

    let stmts = fs.readFileSync('./database/schema.sql').toString().split(/;\s*\n/);

    function next(err) {
        if (err) console.log(err);
        let stmt = stmts.shift();

        if (stmt) app.db.run(stmt, next);
        else if (ready) ready();
    }

    next();
}

createDb();

app.use(function(req, res, next){ res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization'); req.db = app.db; next();});

app.get("/", (req, res, next) => {res.json({"message":"Ok"})});


app.use('/api',postsAPI);
app.use('/api',clientAPI);

app.listen(process.env.PORT || HTTP_PORT, () => {console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT));});
