const express = require('express');

var router = express.Router();

router.post('/log-in', function (req, rsp) {

    let clientData = req.body;

    req.db.get('SELECT * FROM users WHERE email=?', clientData.email, (err, userFromDB) => {
        if (err) throw err;

        if (userFromDB === undefined) {
            rsp.status(404).send('check credentials');
            return;
        }

        if (clientData.email === userFromDB.email) {
            if (clientData.password === userFromDB.password) {
                rsp.status(201).send('' + userFromDB.id);
                return;
            }
        }

        rsp.status(404).send('check password');
        return;
    })

});


router.get('/user/:id',function(req,rsp){

    let userId = req.params.id;

    req.db.get("SELECT * FROM users WHERE id=?", userId, (err, row) => {
        if (err) throw err;

        if (row === undefined) {
            rsp.status(404).send('no such user');
            return;
        }

        console.log(row);
        rsp.status(201).send(row);
    });
});

module.exports = router;
