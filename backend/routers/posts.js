const express = require('express');

var router = express.Router();

router.get('/posts', function (req, rsp) {

    console.log('[GET] posts from API');

    req.db.all('SELECT * FROM posts', (err, posts) => {
        if (err) throw err;

        if(posts.length === 0){
            rsp.status(404).send('no posts were found');
        }

        // Loop through all posts
        posts.forEach((post, index) => {
            // get the id of the writter
            let id = post.userId;

            // get the json object of the writter
            req.db.get('SELECT * FROM users WHERE id=?', id, (err, user) => {
                if (err) throw err;
                post.writter = user;
                if ((posts.length - 1) === index) rsp.status(201).json(posts);
            });
        });
    });
});

router.delete('/posts/:post_id', function (req, rsp) {

    console.log('[DELETE] posts from API');

    let order_id = req.params.post_id;

    req.db.get("SELECT * FROM posts WHERE id=?", order_id, (err, row) => {
        if (err) throw err;

        if (row === undefined) {
            rsp.status(404).send('no post was found');
            return;
        }

        req.db.get('DELETE FROM posts WHERE id=?', order_id, (err) => {
            if (err) throw err;

            rsp.status(201).send('post ' + order_id+' was deleted');
        });
    });
});

router.post('/posts',function(req,rsp){
    
    var data = req.body;

    var sql ='INSERT INTO posts (userId, date, readTime, heading, data, views) VALUES (?,?,?,?,?,?)';
    var params =[data.userId, data.date, data.readTime, data.heading, data.data, data.views];

    req.db.run(sql, params, function (err, result) {

        if (err){
            rsp.status(400).json({"error": err.message})
            return;
        }

        rsp.status(201).send('yo');
    });
});

module.exports = router;
